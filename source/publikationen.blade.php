---
pagination:
    collection: publications
    perPage: 5
---
@extends('_layouts.master')

@push('meta')
    <meta property="og:type" content="website" />
    <meta property="og:url" content="{{ $page->getUrl() }}"/>
    <meta property="og:description" content="Die Liste der Publikationen von {{ $page->siteName }}" />
@endpush

@section('body')
    <h1>Publikationen</h1>

    <hr class="border-b my-6">

    @foreach ($pagination->items as $publication)

        @include('_components.publication-preview-inline')

        @if ($publication !== $pagination->items->last())
            <hr class="border-b my-6">
        @endif
    @endforeach

    @if ($pagination->pages->count() > 1)
        <nav class="flex text-base my-8">
            @if ($previous = $pagination->previous)
                <a
                    href="{{ $previous }}"
                    title="Vorhergehende Seite"
                    class="bg-gray-200 hover:bg-gray-400 rounded mr-3 px-5 py-3"
                >&LeftArrow;</a>
            @endif

            @foreach ($pagination->pages as $pageNumber => $path)
                <a
                    href="{{ $path }}"
                    title="Zu Seite {{ $pageNumber }} gehen"
                    class="hover:bg-gray-400 text-blue-700 rounded mr-3 px-5 py-3 {{
                    $pagination->currentPage === $pageNumber ? 'bg-gray-400' : 'bg-gray-200' }}"
                >{{ $pageNumber }}</a>
            @endforeach

            @if ($next = $pagination->next)
                <a
                    href="{{ $next }}"
                    title="Nächste Seite"
                    class="bg-gray-200 hover:bg-gray-400 rounded mr-3 px-5 py-3"
                >&RightArrow;</a>
            @endif
        </nav>
    @endif
@stop
