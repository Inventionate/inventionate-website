<div class="flex flex-col mb-4">
    <p class="text-gray-700 font-medium my-2">
        @if ($page->getFilename() === "index")
            <i class="fas fa-chalkboard-teacher"></i> Aktueller Vortrag •
        @endif
        {{
            $talk->getDate()
        }}
        •
        {{ $talk->event . " (" . $talk->location . ")" }}
        @if (is_array($talk->author) && count($talk->author) > 1)
            • gemeinsam mit
            @foreach ($talk->author as $author)
                @if ($author !== "Fabian Mundt")
                    {{ $author }}
                @endif
                @if (!$loop->first && !$loop->last && $loop->count > 2)
                    und
                @endif
            @endforeach
        @endif
    </p>

    <h2 class="text-3xl my-0 text-gray-900 font-extrabold">
        {{ $talk->title }}
    </h2>

    <h3 class="text-2xl mt-0 text-gray-900 font-bold">
        {{ $talk->subtitle }}
    </h3>

    <p class="my-0">
        {{ $talk->abstract }}
    </p>

    @if ($talk->url_slides !== null || $talk->url_code !== null || $page->getFilename() === "index")
        <p class="mt-6 mb-2 text-xs">
            @if ($talk->url_slides !== null)
                <a
                    href="{{ $talk->url_slides }}"
                    title="Slides ansehen"
                    class="mr-4 uppercase bg-transparent hover:bg-blue-500 text-blue-700 font-semibold
                    hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent
                    rounded"
                    target="_blank">
                    Slides
                </a>
            @endif
            @if ($talk->url_code !== null)
                <a
                    href="{{ $talk->url_code }}"
                    title="GitLab Repository aufrufen"
                    class="mr-4 uppercase bg-transparent hover:bg-blue-500 text-blue-700 font-semibold
                    hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent
                    rounded"
                    target="_blank">
                    Repository
                </a>
            @endif
            @if ($page->getFilename() === "index")
                <a
                    href="/vortraege"
                    title="Weitere Vorträge ansehen"
                    class="uppercase font-semibold tracking-wide mr-4 bg-blue-500 hover:bg-blue-700
                    text-white hover:text-white border border-blue-500 hover:border-transparent font-bold py-2 px-4 rounded">
                    Weitere Vorträge
                </a>
            @endif
        </p>
    @endif
</div>
