<div class="rounded overflow-hidden shadow-lg">
    <img class="w-full" src="{{ $project->image }}" alt="Projektbild">
    <div class="px-6 py-4">
        <p class="text-gray-700 font-medium my-2">
            @if ($project->featured && $page->getFilename() === "index")
                <i class="fas fa-flag mr-1"></i>
            @endif
            {{ $project->subtitle}} •
            {{ date('Y', $project->start_date) }} bis
            @if (is_string($project->end_date))
                {{ $project->end_date }}
            @else
                {{ date('Y', $project->end_date) }}
            @endif
        </p>
        <h2 class="text-3xl mt-0">
            <a
                href="{{ $project->getUrl() }}"
                title="Über das Promotionsprojekt informieren"
                class="text-gray-900 font-extrabold"
                >
                {{ $project->title }}    
            </a>
        </h2>

        <p class="mb-4 mt-0">
            {!! $project->description !!}
        </p>

        <p class="mb-2 text-xs">
            <a
                href="{{ $project->getUrl() }}"
                title="Über das Promotionsprojekt informieren"
                class="uppercase font-semibold tracking-wide mr-4 bg-blue-500 hover:bg-blue-700 
                text-white hover:text-white border border-blue-500 hover:border-transparent font-bold py-2 px-4 rounded">
                Informieren
            </a>
            @if ($page->getFilename() === "index")
                <a
                    href="/projekte"
                    title="Weitere Projekte ansehen"
                    class="uppercase font-semibold tracking-wide mr-4 bg-blue-500 hover:bg-blue-700 
                    text-white hover:text-white border border-blue-500 hover:border-transparent font-bold py-2 px-4 rounded">
                    Weitere Projekte
                </a>
            @endif
        </p>
    </div>

</div>
