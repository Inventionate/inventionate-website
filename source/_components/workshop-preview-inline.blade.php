<div class="flex flex-col mb-4">
    <p class="text-gray-700 font-medium my-2">
        @if ($page->getFilename() === "index")
            <i class="fas fa-user-friends"></i> Aktueller Workshop • 
        @endif
        {{
            $page->getPeriod($workshop->start_date, $workshop->end_date)
        }}
        •
        {{ $workshop->location }}
        @if (is_array($workshop->author) && count($workshop->author) > 1)
            • gemeinsam mit 
            @foreach ($workshop->author as $author)
                @if ($author !== "Fabian Mundt")
                    {{ $author }}
                @endif
                @if (!$loop->first && !$loop->last && $loop->count > 2)
                    und
                @endif
            @endforeach
        @endif 
    </p>

    <h2 class="text-3xl my-0 text-gray-900 font-extrabold">
        {{ $workshop->title }}
    </h2>

    <h3 class="text-2xl mt-0 text-gray-900 font-bold">
        {{ $workshop->subtitle }}
    </h3>

    <p class="my-0">
        {{ $workshop->abstract }}
    </p>

    @if ($workshop->url_slides !== null || $workshop->url_code !== null || $page->getFilename() === "index")
        <p class="mb-2 mt-6 text-xs">
            @if ($workshop->url_slides !== null)
                <a
                    href="{{ $workshop->url_slides }}"
                    title="Slides ansehen"
                    class="mr-4 uppercase bg-transparent hover:bg-blue-500 text-blue-700 font-semibold 
                    hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent 
                    rounded"
                    target="_blank">
                    Slides
                </a>
            @endif
            @if ($workshop->url_code !== null)
                <a
                    href="{{ $workshop->url_code }}"
                    title="GitLab Repository aufrufen"
                    class="mr-4 uppercase bg-transparent hover:bg-blue-500 text-blue-700 font-semibold 
                    hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent 
                    rounded"
                    target="_blank">
                    Repository
                </a>
            @endif
            @if ($page->getFilename() === "index")
                <a
                    href="/workshops"
                    title="Weitere Workshops ansehen"
                    class="uppercase font-semibold tracking-wide mr-4 bg-blue-500 hover:bg-blue-700 
                    text-white hover:text-white border border-blue-500 hover:border-transparent font-bold py-2 px-4 rounded">
                    Weitere Workshops
                </a>
            @endif
        </p>
    @endif
</div>
