<div class="flex flex-col mb-4">
    <p class="text-gray-700 font-medium my-2">
        @if ($page->getFilename() === "index")
            <i class="fas fa-file-alt"></i> Aktuelle Publikation • 
        @endif
        {{ date( 'Y',
        $publication->date) }}
        @if (is_array($publication->author) && count($publication->author) > 1)
            • gemeinsam mit 
            @foreach ($publication->author as $author)
                @if ($author !== "Fabian Mundt")
                    {{ $author }}
                @endif
                @if (!$loop->first && !$loop->last && $loop->count > 2)
                    und
                @endif
            @endforeach
        @endif 

        @if ($publication->categories)
            @foreach ($publication->categories as $i => $category)
                @if ($loop->index > 0)
                    • 
                @endif
                <a
                    href="{{ '/publikationen/kategorien/' . $category }}"
                    title="Beitrag in {{ $category }} ansehen"
                    class="inline-block bg-gray-300 hover:bg-blue-200 leading-loose tracking-wide
                    text-gray-800 uppercase text-xs font-semibold rounded px-3 pt-px"
                    >{{ $category }}</a>
                @endforeach
            @endif
    </p>

    <h2 class="text-3xl my-0 text-gray-900 font-extrabold">
        {{ $publication->title }}
    </h2>

    <h3 class="text-2xl mt-0 text-gray-900 font-bold">
        {{ $publication->subtitle }}
    </h3>

    <h4 class="mt-0 pt-0 text-xl">
        {{ $publication->publication }}
    </h4>

    <p class="my-0">
        {{ $publication->abstract }}
    </p>

    @if ($publication->url_pdf !== null || $publication->url_code !== null ||
        $publication->url_slides !== null)
        <p class="mb-2 mt-6 text-xs">
            @if ($publication->url_pdf !== null)
                <a
                    href="{{ $publication->url_pdf}}"
                    title="PDF herunterladen"
                    class="mr-4 uppercase bg-transparent hover:bg-blue-500 text-blue-700 font-semibold 
                    hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent 
                    rounded">
                    PDF
                </a>
            @endif
            @if ($publication->url_code !== null)
                <a
                    href="{{ $publication->url_code }}"
                    title="GitLab Repository aufrufen"
                    class="mr-4 uppercase bg-transparent hover:bg-blue-500 text-blue-700 font-semibold 
                    hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent 
                    rounded">
                    Repository
                </a>
            @endif
            @if ($page->getFilename() === "index")
                <a
                    href="/publikationen"
                    title="Weitere Publikationen ansehen"
                    class="uppercase font-semibold tracking-wide mr-4 bg-blue-500 hover:bg-blue-700 
                    text-white hover:text-white border border-blue-500 hover:border-transparent font-bold py-2 px-4 rounded">
                    Weitere Publikationen
                </a>
            @endif
        </p>
    @endif
</div>
