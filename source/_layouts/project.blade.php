@extends('_layouts.master')

@push('meta')
    <meta property="og:title" content="{{ $page->title }}" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="{{ $page->getUrl() }}"/>
    <meta property="og:description" content="{{ $page->description }}" />
@endpush

@section('body')

    @if ($page->image)
        <img src="{{ $page->cover_image ?? $page->image }}" alt="{{ $page->title }} cover image" class="mb-2">
    @endif

    <h1 class="leading-none mb-2">{{ $page->title }}</h1>

    <p class="text-gray-700 text-xl md:mt-0">
        {{ date('Y', $page->start_date) }}
        bis

        @if (is_int($page->end_date))
            {{ date('Y', $page->end_date) }}
        @else
            {{ $page->end_date }}
        @endif

    @if ($page->getProjectsCount($publications))

        • Publikationen: {{ $page->getProjectsCount($publications) }}

    @endif

    @if ($page->getProjectsCount($talks))

        • Vorträge: {{ $page->getProjectsCount($talks) }}

    @endif

    @if ($page->getProjectsCount($workshops))

        • Workshops: {{ $page->getProjectsCount($workshops) }}

    @endif

    </p>

    <div class="mb-10">
        @yield('content')
    </div>

    @foreach ($publications->whereNotNull('projects')->filter(function ($item) use ($page) {
        return in_array($page->title, $item->projects );
        }) as $publication)

        @if ($loop->first)
            <h2 class="mt-12">
                Publikationen
            </h2>
            <hr class="w-full border-b mt-2 mb-6">
        @endif

        @include('_components.publication-preview-inline')

        @if (!$loop->last)
            <hr class="w-full border-b mt-2 mb-6">
        @endif

    @endforeach

    @foreach ($talks->whereNotNull('projects')->filter(function ($item) use ($page) {
        return in_array($page->title, $item->projects);
        }) as $talk)

        @if ($loop->first)
            <h2 class="mt-12">
                Vorträge
            </h2>
            <hr class="w-full border-b mt-2 mb-6">
        @endif

        @include('_components.talk-preview-inline')

        @if (!$loop->last)
            <hr class="w-full border-b mt-2 mb-6">
        @endif

    @endforeach

    @foreach ($workshops->whereNotNull('projects')->filter(function ($item) use ($page) {
        return in_array($page->title, $item->projects);
        }) as $workshop)

        @if ($loop->first)
            <h2 class="mt-12">
                Workshops
            </h2>
            <hr class="w-full border-b mt-2 mb-6">
        @endif

        @include('_components.workshop-preview-inline')

        @if (!$loop->last)
            <hr class="w-full border-b mt-2 mb-6">
        @endif

    @endforeach

    <nav class="flex justify-between text-sm md:text-base">
        <div>
            @if ($next = $page->getNext())
                <a href="{{ $next->getUrl() }}" title="Vorherige Projekte: {{ $next->title }}">
                    &LeftArrow; {{ $next->title }}
                </a>
            @endif
        </div>

        <div>
            @if ($previous = $page->getPrevious())
                <a href="{{ $previous->getUrl() }}" title="Neuere Projekte: {{ $previous->title }}">
                    {{ $previous->title }} &RightArrow;
                </a>
            @endif
        </div>
    </nav>
@endsection
