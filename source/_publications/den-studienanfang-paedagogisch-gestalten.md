---
title: Den Studienanfang pädagogisch gestalten
subtitle: Das Blended Learning Konzept e:t:p:M®
date: 2016-03-01
publication: "In: \"Wie lernt man Erziehen?\" Zur Didaktik der Pädagogik. Herbartstudien. Bd. 6. Hg. von Rainer Bolle und Wolfgang Halbeis. Jena: Garamond. S. 219–233"
author: ["Timo Hoyer", "Fabian Mundt"]
categories: [artikel]
projects: ["e:t:p:M"]
---
