---
title: Blended Evaluation in digital gestützter Lehre
date: 2018-05-01
publication: "In: Digitalisierung der Hochschullehre. Neue Anforderungen an die Evaluation? Hg. von Susan Harris-Huemmert, Philipp Pohlenz & Lukas Mitterauer. Münster: Waxmann. 2018"
url_pdf: http://www.waxmann.com/buch3807
url_code: https://github.com/inventionate/learning-analytics
author: ["Fabian Mundt", "Timo Hoyer"]
categories: [artikel]
projects: ["e:t:p:M"]
---
