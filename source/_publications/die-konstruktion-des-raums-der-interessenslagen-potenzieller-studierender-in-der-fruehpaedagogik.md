---
title: Die Konstruktion des Raums der Interessenslagen potenzieller Studierender in der Frühpädagogik
date: 2017-09-12
publication: "In: die hochschullehre. Jg. 3|2017"
url_pdf: "http://www.hochschullehre.org/wp-content/files/diehochschullehre_2017_Mundt-Kutzner-Konstruktion_des_Raums_der_Interessenslagen_potenzieller_Studierender.pdf"
author: ["Fabian Mundt", "Daniela Kutzner"]
categories: [artikel, peer-review]
projects:
    - TimeSpaceAnalysis
---
