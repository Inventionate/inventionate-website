---
title: The Blended Learning Concept e:t:p:M@Math
subtitle: Practical Insights and Research Findings
date: 2018-05-01
publication: "In: Distance Learning, E-Learning and Blended Learning in Mathematics Education. Hg. von Jason Silverman und Veronica Hoyos. ICME-13 Monographs. New York: Springer"
author: ["Fabian Mundt", "Mutfried Hartmann"]
categories: [artikel, peer-review]
projects: ["e:t:p:M"]
---
