---
title: Old Habits Die Hard
subtitle: Charakterbildung und Habitus oder die Möglichkeiten reflexiver Erziehung
date: 2011-03-03
publication: "Berliner Arbeiten zur Erziehungs- und Kulturwissenschaft. Bd. 58. Berlin: Logos"
abstract: "Die Begriffe Habitus und Charakter weisen vielfache Zusammenhänge auf. Diese Arbeit verfolgt durch die Systematisierung beider Begriffe die Möglichkeiten einer reflexiven Erziehung zu skizzieren. Zu diesem Zweck wird die Theorie der Charakterbildung von Johann Friedrich Herbart in Beziehung zur Habitustheorie von Pierre Bourdieu gesetzt. Es zeigt sich, dass der Charakter als Kern des Habitus betrachtet werden kann. Letztlich geht es um Einsicht in die Notwendigkeit von Charakterbildung, die um die Rahmenbedingungen und Eigenarten von Habitualisierungsvorgängen weiß."
categories: [monografie]
---
