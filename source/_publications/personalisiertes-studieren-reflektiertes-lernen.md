---
title: Personalisiertes Studieren, reflektiertes Lernen
subtitle: Eine Analyse des Studierverhaltens in digital gestützter Lehre
date: 2017-12-01
publication: "In: Erziehungswissenschaft. Bd. 55. Jg. 28. Themenschwerpunkt \"Universität 4.0\". S. 61–72"
url_pdf: "http://www.dgfe.de/fileadmin/OrdnerRedakteure/Zeitschrift_Erziehungswissenschaft/EW_55.pdf"
url_code: "https://github.com/inventionate/learning-analytics"
author: ["Timo Hoyer", "Fabian Mundt"]
categories: [artikel]
projects: ["e:t:p:M"]
---
