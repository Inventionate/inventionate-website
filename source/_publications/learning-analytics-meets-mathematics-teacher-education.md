---
title: Learning Analytics meets Mathematics Teacher Education
subtitle: Digital geschärfte Einblicke in das Lernverhalten zu Beginn des Lehramtsstudiums
date: 2017-02-01
publication: "In: Beiträge zum Mathematikunterricht 2017. Hg. von U. Kortenkamp & A. Kuzle. Münster: WTM"
categories: [artikel]
author: ["Fabian Mundt", "Mutfried Hartmann"]
projects: ["e:t:p:M"]
---
