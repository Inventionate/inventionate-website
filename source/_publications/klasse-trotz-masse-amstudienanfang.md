---
title: Klasse trotz Masse am Studienanfang
subtitle: Das Blended-Learning-Konzept e:t:p:M@Math
date: 2015-02-01
publication: "In: Beiträge zum Mathematikunterricht 2015. Hg. von Helmut Linneweber-Lammerskitten. Münster: WTM"
author: ["Fabian Mundt", "Mutfried Hartmann"]
categories: [artikel]
projects: ["e:t:p:M"]
---
