---
title: Exploring the Complex Social Spaces of Higher Education
subtitle: On the Uses and Challenges of Geometric Data Analysis and Topological Approaches
date: 2019-01-01
publication: "In: Theory and Method in Higher Education Research. Bd. 4"
url_code: "https://github.com/inventionate/exploring-student-space-higher-education"
author: ["Fabian Mundt", "Kenneth Horvath"]
categories: [artikel]
projects: 
    - "Zeit-Raum Studium"
    - TimeSpaceAnalysis
abstract: "In this paper, we propose a specific mixed-methods approach for exploring the field of higher education that starts from the concept of social space (Lefebvre 1992; Bourdieu 1995). The notion of social space offers a useful heuristic for a number of problems relevant to higher education research: it shifts our attention to systems of relations between social actors (spatialization), to how these relations become stabilized in and through social and cultural practices, and to the processes of subjectification and positioning linked to these sets of relations. Correspondingly, it has been used rather widely, especially in studies that draw on Bourdieusian relational sociology of education (e.g. Börjesson et al. 2016, Lebaron & Le Roux 2015, Larsen & Beech 2014, Grenfell & Lebaron 2014)."
---
