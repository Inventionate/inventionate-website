---
title: Apps Im Mathematikunterricht
date: 2014-03-01
publication: "In: Beiträge zum Mathematikunterricht 2014. Bd. 1. Beiträge zur 48. Jahrestagung der Gesellschaft für Didaktik der Mathematik. Hg. von Engelbert Niehaus et al. Münster: WTM. S. 217–220"
author: ["Fabian Mundt", "Thomas Borys"]
categories: [artikel]
projects: ["app@school"]
---
