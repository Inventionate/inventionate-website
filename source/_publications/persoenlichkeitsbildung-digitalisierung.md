---
title: Persönlichkeitsbildung in digitalisierten Zeiten
author: 
  - Fabian Mundt
  - Gabriele Weigand
date: 2018-06-01
publication: "In: bbw Beruflicher Bildungsweg. Bd. 6|2018. Jg. 59"
categories: [artikel]
---
