---
title: Bildsamkeit und Habitus
date: 2014-03-01
publication: "In \"Einheimische Begriffe\" und Disziplinentwicklung. Herbartstudien. Band 5. Hg. von Rotraud Coriand und Alexandra Schotte. Garamond: Jena. S. 307–321"
url: "http://www.garamond-verlag.de/shop/Paedagogik-Bildung/Einheimische-Begriffe-und-Disziplinentwicklung-Herbartstudien-Bd-5::273.html"
categories: [artikel]
---
