---
title: app@school  
subtitle: App Entwicklung im Unterricht, aber wie?
date: 2015-02-01
publication: "In: Beiträge zum Mathematikunterricht 2015. Hg. von Helmut Linneweber-Lammerskitten. Münster: WTM"
author: ["Fabian Mundt", "Thomas Borys"]
categories: [artikel]
projects: ["app@school"]
---
