---
title: e:t:p:M
subtitle: Ein Blended-Learning-Konzept für Großveranstaltungen
date: 2014-09-01
publication: "In: Lernräume gestalten – Bildungskontexte vielfältig denken. GMW Tagungsband. Hg. von Klaus Rummler. Münster: Waxmann. S. 249–259"
author: ["Timo Hoyer", "Fabian Mundt"]
categories: [artikel, peer-review]
projects: ["e:t:p:M"]
---
