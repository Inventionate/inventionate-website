---
title: R Essentials Personal Coaching
subtitle: Wie man effizienten und eleganten R-Code schreibt
start_date: 2020-10-28
end_date: 2020-10-28
location: Campus Luzern (Webinar)
abstract: R ist ein mächtiger und flexibler Ersatz für kommerzielle Statistikpakete. Wer die Potentiale von R voll ausschöpfen und langfristig Freude an der Arbeit mit R haben will, sollte früh damit beginnen, R als Programmiersprache zu verstehen und zu nutzen. In diesem individuellen Coaching werden aktuelle Paradigmen auf eigene Projekte und Arbeitsweisen bezogen.
projects: 
    - R Essentials
---
