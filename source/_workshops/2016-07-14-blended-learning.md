---
title: Blended Learning
subtitle: Einführung in die Theorie und Praxis
start_date: 2016-07-14
end_date: 2016-07-14
location: Zentrum für Informationstechnologie und Medien (PH Karlsruhe)
author:
    - Fabian Mundt
    - Stefan Weber
---
