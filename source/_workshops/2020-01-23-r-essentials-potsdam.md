---
title: R Essentials Focus
subtitle: Allgemeine Einführung in Data Science Arbeitsweisen
start_date: 2020-01-23
end_date: 2020-01-24
location: Centre for Educational Research in Mathematics and Technology (Universität Potsdam)
abstract: Der Workshop vermittelt die Basiskonzepte und grundlegende Arbeitstechniken in R. Ein besonderes Augenmerk liegt dabei auf aktuellen Entwicklungen, beispielsweise dem Tidyverse. Die Inhalte werden dabei speziell an den Bedürfnissen der Arbeitsgruppe vor Ort ausgerichtet.
projects: 
    - R Essentials
---
