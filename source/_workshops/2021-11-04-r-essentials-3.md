---
title: R Essentials 3
subtitle: Wie man effizienten und eleganten R-Code schreibt
start_date: 2021-11-04
end_date: 2021-11-05
location: Campus Luzern
abstract: R ist ein mächtiger und flexibler Ersatz für kommerzielle Statistikpakete. Wer die Potentiale von R voll ausschöpfen und langfristig Freude an der Arbeit mit R haben will, sollte früh damit beginnen, R als Programmiersprache zu verstehen und zu nutzen.
projects: 
    - R Essentials
---
