---
title: R Essentials 2
subtitle: Wie man R für Grafiken und Visualisierungen nutzt
start_date: 2021-05-20
end_date: 2021-05-21
location: Campus Luzern (Webinar)
abstract: Eine der zentralen Stärken von R liegt in den vielfältigen und flexiblen Visualisierungsmöglichkeiten, von den ersten Schritten der grafischen Exploration von Datensätzen bis zur publikationsreifen Ergebnispräsentation. Der Workshop gibt einen Überblick über diese Möglichkeiten und diskutiert die grundlegenden Logiken und konkreten Techniken der Datenvisualierung in R. 
author:
    - Kenneth Horvath
    - Fabian Mundt
projects: 
    - R Essentials
---
