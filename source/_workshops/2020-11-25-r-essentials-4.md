---
title: R Essentials 4
subtitle: Das Tidyverse
start_date: 2020-11-25
end_date: 2020-11-26
location: Campus Luzern
abstract: "R ist das Ergebnis jahrzehntelanger Entwicklungsarbeit. Das Tidyverse ist eine der
jüngsten und für viele Nutzungsformen eine der wichtigsten Neuerungen im Universum von R. Es handelt
sich um eine Sammlung von Paketen, die einem Set von Prinzipien folgen, die leichte Verständlichkeit
und effizientes Arbeiten garantieren sollen. Häufig werden konventionelle R-Funktionen neu
interpretiert und modernen Ansätzen folgend optimiert. Speziell für die Aufbereitung und das
Management von Datensätzen ergeben sich dabei neue, komfortable und sehr performante Möglichkeiten.
Aber auch andere Aspekte wie das Arbeiten mit Text/Strings oder mächtige Techniken der
Visualisierung sind Teil des Tidyverse. Der Workshop führt anhand konkreter Beispiele in die Logiken
und Komponenten des Tidyverse ein und erläutert deren Nutzen für die tägliche Arbeit mit R."
author:
    - Kenneth Horvath
    - Fabian Mundt
projects: 
    - R Essentials
---
