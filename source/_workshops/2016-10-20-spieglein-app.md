---
title: Spieglein, Spieglein in der … App
subtitle: Symmetrie	erforschen mit digitalen Medien
start_date: 2016-10-20
end_date: 2016-10-20
location: Auftaktveranstaltung Mathe.Forscher (Heilbronn)
author:
    - Christiane Benz
    - Fabian Mundt
---
