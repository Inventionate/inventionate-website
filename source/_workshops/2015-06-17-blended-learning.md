---
title: Blended Learning
subtitle: Einführung in die Theorie und Praxis
start_date: 2015-06-17
end_date: 2015-06-17
location: Zentrum für Informationstechnologie und Medien (PH Karlsruhe)
author:
    - Fabian Mundt
    - Stefan Weber
---
