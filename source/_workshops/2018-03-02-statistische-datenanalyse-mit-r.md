---
title: Statistische Datenanalyse mit R
subtitle: Basiskonzepte und praxisnahe Anwendungen
start_date: 2018-03-02
end_date: 2018-03-02
location: Winterakademie der Graduiertenakademie der Pädagogischen Hochschulen (Bad Herrenalb)
author:
    - Kenneth Horvath
    - Fabian Mundt
---
