---
title: Zeit-Raum Studium
subtitle: Eine Sozioanalyse über Studierende in der Studieneingangsphase
event: HoFoNa Ideenforum, GfHf Jahrestagung
date: 2016-04-07
location: Kath. Akademie Bayern
url_slides: "http://www.slideshare.net/Inventionate/zeitraum-studium-eine-sozioanalyse-ber-studierende-in-der-studieneingangsphase"
projects: 
    - "Zeit-Raum Studium"
---

