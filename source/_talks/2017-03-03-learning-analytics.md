---
title: Learning Analytics meets Mathematics Teacher Education
subtitle: Digital geschärfte Einblicke in das Lernverhalten zu Beginn des Lehramtsstudiums
event: GDM Jahrestagung
date: 2017-03-0
location: Universität Potsdam
url_slides: http://www.slideshare.net/Inventionate/quality-despite-quantity
author:
    - Fabian Mundt
    - Mutfried Hartmann
projects: 
    - "e:t:p:M"
---

