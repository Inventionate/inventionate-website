---
title: "app@school"
event: "GDM Jahrestagung"
date: 2015-02-13
location: "Universität Basel"
url_slides: "https://www.slideshare.net/Inventionate/appschool"
author:
    - Fabian Mundt
    - Thomas Borys
projects: 
    - "app@school"
---

