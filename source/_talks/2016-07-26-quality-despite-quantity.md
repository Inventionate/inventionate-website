---
title: »Quality Despite Quantity«
subtitle: "The e:t:p:M@Math concept for blended learning at the beginning of mathematical studies"
event: "ICME 13, Distance Learning Panel"
date: 2016-07-26
location: Universität Hamburg
url_slides: "http://www.slideshare.net/Inventionate/quality-despite-quantity"
author:
    - Fabian Mundt
    - Mutfried Hartmann
projects: 
    - "e:t:p:M"
---

