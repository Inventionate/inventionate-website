---
title: Heterogenität in Bewegung
subtitle: Zur Produktion und Reproduktion studentischer Zeit-Raum-Verhältnisse in der Studieneingangsphase
event: graph Zukunftsforum Bildungsforschung
date: 2016-11-26
location: PH Ludwigsburg
url_slides: http://www.slideshare.net/Inventionate/heterogenitt-in-bewegung
projects: 
    - "Zeit-Raum Studium"
---

