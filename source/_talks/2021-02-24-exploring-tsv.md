---
title: "Exploring Temporal-Spatial Visualizations"
subtitle: "Of Space-Time Cubes, Choropleths and Places Chronologies"
event: "R User Group Lucerne"
date: 2021-02-24
location: Webkonferenz
abstract: "The talk traces the visualization possibilities of temporal-spatial data. After some exemplary representations have been discussed, the focus is on the own research and the exploration of so-called places chronologies."
projects: 
    - "Zeit-Raum Studium"
---

