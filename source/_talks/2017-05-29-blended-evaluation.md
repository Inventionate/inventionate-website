---
title: Blended Evaluation in der digital gestützten Lehre
event: "DeGEval Frühjahrstagung \"Digitalisierung der Hochschullehre\""
date: 2017-05-29
location: Universität Wien
author:
    - Fabian Mundt
    - Timo Hoyer
projects: 
    - "e:t:p:M"
---

