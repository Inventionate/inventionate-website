---
title: Die Konstruktion einer sozialen Raumkarte des Studierverhaltens
subtitle: Möglichkeiten und Grenzen der Geometrischen Datenanalyse
event: "Institut für Forschungsmethoden: Ateliergespräch"
date: 2018-12-12
location: PH Karlsruhe
abstract: "Im Mittelpunkt des Vortrags steht die forschungspraktische Anwendung der Geometrischen Datenanalyse (Le Roux & Rouanet 2004; Le Roux 2014). Dabei handelt es sich um ein statistisches Framework, das eine profunde Alternative zu gemeinhin gängigen Verfahrensweisen darstellt (vgl. Blasius & Greenacre 2014)."
projects: 
    - "Zeit-Raum Studium"
---

