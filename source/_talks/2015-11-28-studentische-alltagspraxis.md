---
title: Die studentische Alltagspraxis als Reflexionsangebot
subtitle: Eine Sozioanalyse der Zeit-Raum-Verhältnisse von Lehramtsstudierenden
event: graph Zukunftsforum Bildungsforschung
date: 2015-11-28
location: "PH Heidelberg"
projects: 
    - "Zeit-Raum Studium"
---

