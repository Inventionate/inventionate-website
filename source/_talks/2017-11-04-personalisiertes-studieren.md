---
title: Reflektiertes Lernen, personalisiertes Studieren
subtitle: Eine Analyse des Studierverhaltens in digital gestützter Lehre
event: "DGfE Tagung »Universität 4.0«"
date: 2017-11-04
location: "Neue Mälzerei, Berlin"
url_slides: https://www.slideshare.net/secret/nTZz95fGcRCzt5
author:
    - Timo Hoyer
    - Fabian Mundt
projects: 
    - "e:t:p:M"
---

