---
extends: _layouts.publication-category
title: Monografien
description: Alle bisher veröffentlichten Bücher.
---

Alle bisher veröffentlichten Monografien.
