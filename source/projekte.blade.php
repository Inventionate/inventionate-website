@extends('_layouts.master')

@push('meta')
    <meta property="og:type" content="website" />
    <meta property="og:url" content="{{ $page->getUrl() }}"/>
    <meta property="og:description" content="Die Liste der Publikationen von {{ $page->siteName }}" />
@endpush

@section('body')
    <h1>Projekte</h1>

    <hr class="border-b my-6">

    <div class="grid grid-col-1 md:grid-cols-2 gap-12 pt-4">

        @foreach ($projects as $project)

            @include('_components.project-preview-inline')

        @endforeach

    </div>
@stop
