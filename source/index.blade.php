@extends('_layouts.master')

@section('body')

    <div class="flex flex-col md:flex-row">

        <div class="w-full md:w-2/3 md:mr-10">

            <h1>Willkommen</h1>

            <p class="mb-6">
                Hier finden Sie Informationen rund um meine Forschungs-, Lehr- und
                Entwicklungstätigkeiten.
            </p>

            <p class="mb-6">
                Momentan befasse ich mich einerseits mit meiner <b>Promotion</b>, in deren Rahmen ich
                <b>Zeit–Raum-Konstellationen</b> konzeptuell und methodologisch erkunde
                (<a href="/projekte/zeit-raum-studium">Zeit–Raum Analyse</a>). Hierzu greife
                ich auf geoinformatische, statistische und sozialwissenschaftliche Ansätze zurück. Andererseits arbeite
                ich im Kontext <b>Digitalisierung</b> der Hochschule an der technologischen wie didaktischen Umsetzung
                innovativer Lehr-Lern-Formate (z. B. <a href="/projekte/etpM">e:t:p:M</a>), der Implementierung moderner
                Lösungen des Forschungsdatenmanagements (z. B. <a href="/projekte/lemas-cloud">LemaS Cloud Platform</a>)
                sowie der Vermittlung aktueller Analyse- und Visualisierungstechniken (z. B.
                <a href="/projekte/r-essentials">R Essentials</a>).
            </p>

            <p>
                Als digitale Werkzeuge nutze ich hauptsächlich
                <a target="_blank" href="https://pandoc.org">Pandoc <i class="fas fa-external-link-alt"></i></a>,
                <a target="_blank" href="http://luatex.org/">LuaTeX <i class="fas fa-external-link-alt"></i></a>,
                <a target="_blank" href="https://www.r-project.org">R <i class="fas fa-external-link-alt"></i></a> und den
                <a target="_blank" href="https://tallstack.dev">TALL Stack <i class="fas fa-external-link-alt"></i></a>.
                Mein bevorzugter Editor ist
                <a target="_blank" href="https://neovim.io">Neovim <i class="fas fa-external-link-alt"></i></a>
                (<a target="_blank" href="https://gitlab.com/Inventionate/dotfiles">dotfiles <i class="fas fa-external-link-alt"></i></a>).
            </p>

        </div>

        <div class="w-full md:w-1/3 md:ml-6">

            <img src="/assets/img/avatar.jpg"
                 alt="Fabian Mundt avatar"
                 class="flex justify-center rounded-full shadow-lg h-64 w-64 bg-contain mx-auto mb-6">

            <ul class="list-none flex justify-center text-5xl" aria-hidden="true">

                <li class="mr-8">
                    <a href="m&#97;ilto&#58;%66&#46;&#109;&#37;7&#53;&#110;%64&#37;&#55;&#52;&#64;i&#110;ve&#110;&#116;ion&#97;te&#37;2E%64%65">
                        <i class="fa fa-paper-plane transition duration-500 ease-in-out transform
                        hover:scale-110"></i>
                    </a>
                </li>

                <li class="mr-8">
                    <a href="//gitlab.com/Inventionate">
                        <i class="fab fa-gitlab transition duration-500 ease-in-out transform
                        hover:scale-110"></i>
                    </a>
                </li>

                <li>
                    <a href="//researchgate.net/profile/Fabian_Mundt">
                        <i class="fab fa-researchgate transition duration-500 ease-in-out transform
                        hover:scale-110"></i>
                    </a>
                </li>

            </ul>

        </div>

    </div>

    <hr class="border-b my-6">

    <div class="w-full mb-6">

        @foreach($publications->take(1) as $publication)

            @include('_components/publication-preview-inline')

        @endforeach

    </div>

    <hr class="border-b mb-6 mt-10">

    <div class="w-full mb-6">

        @foreach($workshops->take(1) as $workshop)

            @include('_components/workshop-preview-inline')

        @endforeach

    </div>

    <hr class="border-b mb-6 mt-10">

    <div class="w-full mb-4">

        @foreach($talks->take(1) as $talk)

            @include('_components/talk-preview-inline')

        @endforeach

    </div>

    <hr class="border-b mb-6 mt-10">

    <div class="grid grid-col-1 md:grid-cols-2 gap-12 pt-4">

        @foreach ($projects->where('featured', true) as $project)

            @include('_components.project-preview-inline')

        @endforeach

    </div>

@endsection
