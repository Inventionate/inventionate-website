---
extends: _layouts.project
section: content
title: TimeSpaceAnalysis
subtitle: Ein R Paket für die sozialwissenschaftliche Zeit-Raum-Analyse
start_date: 2015-06-01
description: Das Paket implementiert eine Vielzahl von Techniken aus der Geometrischen Datenanalyse, vor allem Verfahren rund um die multiple Korrespondenzanalyse. Darüber hinaus bietet es GIS-Funktionen, um sogenannte Orte Chronologien zu erstellen und auszuwerten, und Methoden zur Analyse von Zeitmustern.
image: "/assets/img/tsa.png"
---

Das R Paket [TimeSpaceAnalysis](https://gitlab.com/Inventionate/TimeSpaceAnalysis) implementiert eine Vielzahl von Techniken aus der Geometrischen Datenanalyse, vor allem Verfahren rund um die multiple Korrespondenzanalyse. Darüber hinaus bietet es GIS-Funktionen, um sogenannte Orte Chronologien zu erstellen und auszuwerten, und Methoden zur Analyse von Zeitmustern.

![TSA Beispiel](/assets/img/tsa-example.png)

Die Visualisierung der Ergebnisse erfolgt in **ggplot2**. Alle Funktionen sind ausführlich dokumentiert. Das Paket befindet sich aktuell unter aktiver Entwicklung und wurde bisher noch nicht auf CRAN veröffentlicht. Eine Nutzung ist allerdings bereits möglich.

```r
install.packages("remotes")
remotes::install_gitlab("inventionate/TimeSpaceAnalysis")
```

> **Info:** TimeSpaceAnalysis nutzt zur Auswertung von Daten vor allem Funktionen der Pakete [factoextra](http://www.sthda.com/english/wiki/factoextra-r-package-easy-multivariate-data-analyses-and-elegant-visualization), [FactoMineR](http://factominer.free.fr) (beiden Paketen wurden im Zuge der TimeSpaceAnalysis-Entwicklung weitere Funktionen hinzugefügt) und [kml / kml3d](https://cran.r-project.org/web/packages/kml/index.html).
