---
extends: _layouts.project
section: content
title: Zeit-Raum Studium
subtitle: Promotionsprojekt
start_date: 2015-06-01
description: "Im Rahmen dieser Arbeit wird die These vertreten, dass die Erkundung der Zeit-Raum-Verhältnisse von Studierenden weitreichende und detaillierte Erkenntnisse über deren Hintergründe, Lebenssituation und Veränderungsvorgänge im Laufe ihres Studiums ermöglicht. Die forschungsleitende Fragestellung lautet: Wie erfahren und gestalten Studierende ihren Zeit-Raum Studium?"
image: "https://images.unsplash.com/photo-1501139083538-0139583c060f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1950&q=80"
featured: true
---

> **Info:** Für das Projekt existiert eine Open Science Framework (OSF) Seite, auf der konkrete Daten, Ergebnisse und die dafür entwickelte Software zusammengestellt sind: [http://osf.io/37znf](http://osf.io/37znf).

Im Rahmen des Forschungsprojekts wird nach der (Re-)Produktion studentischer Zeit-Raum-Verhältnisse gefragt. Deren Rekonstruktion ermöglicht eine dynamische und kritische Perspektive auf die Heterogenität der Studierendenschaft. Anstatt einzelne Differenzmerkmale wie Alter, Geschlecht, Milieu oder Ethnie hervorzuheben, fokussiert die Rekonstruktion von Zeit-Raum-Verhältnissen deren relationale Verflechtungen. Dieses Vorgehen liefert detaillierte Erkenntnisse über die Vielfalt der Studierenden, die eindimensional-statischen Konzeptualisierungen verborgen bleiben.

Zunächst werden Zeit-Raum-Verhältnisse als forschungsleitende Heuristik thematisiert. Zeit und Raum werden als soziale Konstruktionen verstanden, die zugleich materiale Komponenten aufweisen. Zeitlich-räumliche Konfiguration lassen sich als temporal variable relationale Ordnungen sozialer Güter und Menschen beschreiben, die durch soziale Praktiken konstituiert werden. Die Rekonstruktion und Analyse von Zeit-Raum-Verhältnisse erfolgen sowohl »mental« (Wahrnehmungen, Empfindungen) als auch »metrisch« (Entfernungen, Tageszeiten).

Im Anschluss an diese Überlegungen wird eine Forschungsstrategie zur Diskussion gestellt, die die konzeptionelle Heuristik in ein methodisches Längsschnittdesign übersetzt.  Ziel ist es, heterogene Zeit-Raum-Verhältnisse in ihrer temporalen (Re-)Produktion sichtbar zu machen. Um diesen Anspruch einzulösen, wird auf Verfahren der Geometrischen Datenanalyse zurückgegriffen. Geometrische Methoden, wie die multiple Korrespondenzanalyse, ermöglichen die Visualisierung der studentischen Alltagspraxis zu einem bestimmten Zeitpunkt. Es wird ein Abbild der relationalen Verflechtungen der Studierenden generiert – ein sozialer Raum –, der die Grundlage für eine erste Interpretation bildet. Eine weiterführende Interpretation wird durch die Verzeitlichung des Raums eröffnet. Indem die Ergebnisse eines zweiten, späteren Erhebungszeitpunkts mit in die geometrische Analyse aufgenommen werden, gerät die temporale Variation des sozialen Raums in den Blick.

Die forschungsstrategischen Überlegungen werden abschließend anhand der Daten, die durch drei Befragungen der ersten BA Education Student/innen an der PH Karlsruhe gewonnen wurden, mit konkreten Ergebnissen geprüft. Es werden sowohl empirisch begründete Antworten auf die Frage nach der (Re-)Produktion studentischer Zeit-Raum-Verhältnisse präsentiert als auch die forschungspraktische Durchführung kritisch reflektiert.
