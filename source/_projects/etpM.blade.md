---
extends: _layouts.project
section: content
title: e:t:p:M
subtitle: Ein modernes Blended Learning Konzept
start_date: 2012-06-01
end_date: 2020-10-31
description: Wie leben in digitalisierten Zeiten. An Hochschulen ist davon alleridngs noch nicht sehr viel zu sehen. Das e:t:p:M-Konzept nutzt die Möglichkeiten moderner Technologien und verbindet diese mit einem innovativen hochschuldidaktischen Blended Leaning-Ansatz. Ziel ist es, die herausfordernde Studieneingangsphase pädagogischer zu gestalten.
image: "/assets/img/etpM.png"
cover_image: "/assets/img/responsive-webapp.png"
---

Das Blended Learning Konzept e:t:p:M ist für regelmäßig stattfindende Lehrveranstaltungen (z.B. Einführungsvorlesungen) mit großer und sehr großer Teilnehmerzahl von Apl. Prof. Dr. Timo Hoyer und Fabian Mundt, M.A. entwickelt worden. Es trägt in besonderem Maße dem Umstand Rechnung, dass sich die Studierenden am Übergang von der schulischen zur akademischen Lehr-Lern-Kultur befinden.

<img src="/assets/img/etpm-poster.jpeg" alt="e:t:p:M Poster" style="width: 100%" />

> Eine ausführliche Darstellung des Gesamtkonzepts finden Sie im Sammelband [Lernräume gestalten – Bildungskontexte vielfältig denken (2014)](http://www.waxmann.com/index.php?id=buecher&no_cache=1&tx_p2waxmann_pi1%5Boberkategorie%5D=OKA100022&tx_p2waxmann_pi1%5Breihe%5D=REI100145&tx_p2waxmann_pi1%5Bbuch%5D=BUC123898).

<video class="w-full border-none" controls>
  <source src="/assets/video/promo.mp4" type="video/mp4">
  <source src="/assets/video/promo.webm" type="video/webm">
</video>
