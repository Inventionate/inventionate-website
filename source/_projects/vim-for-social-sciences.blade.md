---
extends: _layouts.project
section: content
title: Vim for the Social Sciences
subtitle: Ein Startpaket zur Nutzung von Vim in den Sozialwissenschaften
start_date: 2018-10-26
description: Das Projekt stellt – inspiriert durch das <a href="https://kieranhealy.org/resources/emacs-starter-kit/" target="_blank">Emacs Starter Kit</a> – ein Vim Starter Kit für den Einsatz in den Sozialwissenschaften bereit. Es ermöglicht auf einfache Art und Weise eine komfortable Arbeit mit längeren Texten, die neben einer erweiterten Grammatikprüfung, Zitat-Autovervollstöndigung und einem speziellen Konzentrationsmodus mit vielen weiteren Erleichterungen aufwarten kann.
image: "/assets/img/vim.png"
---

Das Projekt stellt – inspiriert durch das <a href="https://kieranhealy.org/resources/emacs-starter-kit/" target="_blank">Emacs Starter Kit</a> – ein Vim Starter Kit für den Einsatz in den Sozialwissenschaften bereit. Es ermöglicht auf einfache Art und Weise eine komfortable Arbeit mit längeren Texten, die neben einer erweiterten Grammatikprüfung, Zitat-Autovervollstöndigung und einem speziellen Konzentrationsmodus mit vielen weiteren Erleichterungen aufwarten kann.

> **Info:** Aktuell lässt sich die Vim Konfiguration nur im Rahmen allgemeiner [dotfiles](https://gitlab.com/Inventionate/dotfiles) herstellen, die auch Einstellungen für einige Programmiersprachen beinhalten. Eine spezielle Repository mit der expliziten Konfiguration von Vim für den Einsatz in den Sozialwissenschaften ist geplant.
