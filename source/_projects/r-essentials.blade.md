---
extends: _layouts.project
section: content
title: R Essentials
subtitle: Workshop Reihe
start_date: 2019-01-01
description: Die Programmiersprache R hat insbesondere in den letzten Jahren an Popularität und Bedeutung gewonnen. In Zeiten von Big Data und dem wachsenden Bedarf an hochwertigen Visualisierungen spielt R seine Stärken aus. Die Workshop Reihe greift diese Entwicklungen auf und konzentriert sich anwendungsorientiert auf die Vermittlung von Kernkonzepten und modernen Basistechniken.
image: "https://images.unsplash.com/photo-1542903660-eedba2cda473?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1950&q=80"
featured: true
---

> **Info:** Die R Essentials sind Workshops, die von [Guy Schwegler](https://www.unilu.ch/fakultaeten/ksf/institute/soziologisches-seminar/mitarbeitende/guy-schwegler-ma/), [Kenneth Horvath](https://www.unilu.ch/fakultaeten/ksf/institute/soziologisches-seminar/mitarbeitende/dr-kenneth-horvath/) und [Fabian Mundt]() vor allem am [Campus Luzern](https://www.campus-luzern.ch/kurse/r-kurse-fs/) durchgeführt werden.
