---
extends: _layouts.project
section: content
title: LemaS Cloud Platform
subtitle: Eine umfassende und integrative IT-Infrastruktur
start_date: 2018-01-01
description: Die LemaS Cloud Platform stellt die digitale Lösung des Entwicklungs- und Forschungsprojekts »Leistung macht Schule« dar. Es kombiniert eine Laravel basierte Website mit einer modernen Forschungs- und Produktdatenbank, einer Nextcloud-Instanz, einem R Shiny Server, einer LimeSurvey-Instanz und einem LMS (Moodle). 
image: "https://images.unsplash.com/photo-1566443280617-35db331c54fb?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1867&q=80" 
---

Die LemaS Cloud Platform kann als Überbegriff der gesamten IT Infrastruktur des [Forschungs- und Entwicklungsprojekts LemaS](https://www.lemas-forschung.de) aufgefasst werden. Im Kern besteht sie aus einer Nextcloud-Instanz, die zugleich als primärer Authentifizierungsdienst fungiert. Neben weiteren Systemen, wie einem LMS (Moodle) und einem R Shiny Server, ist es vor allem eine speziell entwickelte Forschungs- und Produktdatenbank auf Basis von [Laravel](https://www.laravel.com), die ein tief integriertes Gesamtsystem ermöglicht. Die Datenbank integriert beispielsweise statistische Metriken und ist in der Lage dynamische Dokumente in verschiedenen Formaten automatisch zu erzeugen. Auch interaktive Webapps auf Basis von R werden hierbei verwendet.
