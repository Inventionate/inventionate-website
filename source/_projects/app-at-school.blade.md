---
extends: _layouts.project
section: content
title: app@school
subtitle: App Enwticklung in der Schule
start_date: 2014-02-01
end_date: 2015-12-31
description: Apps für Mobile Devices durchdringen unseren Alltag. Im Unterrichtsalltag sind sie als Lerngegenstand hingegen selten anzutreffen. app@school will daran etwas ändern und macht eine Schulklasse zum Entwicklungsteam.
image: "/assets/img/colourize.jpg"
---

Der Kerngedanke von app@school besteht darin, dass die Schülerinnen und Schüler »die Seite wechseln« und selbst eine Anwendung konzipieren, entwickeln und veröffentlichen. 
Mit Blick auf die Verbreitung und alltägliche Nutzung mobiler Endgeräte haben wir festgelegt, dass es sich dabei um eine Spiele-App für Smartphones und Tablets handeln soll. 
Als Lehr-Lern-Szenario greift app@school einerseits den pädagogischen Projektgedanken auf, zielt also auf prozessuales Lernen ab, und orientiert sich andererseits an den Abläufen agiler Softwareentwicklung und den damit verbundenen Arbeitsstrukturen. 
In Anlehnung an das zur Zeit viel diskutierte _Game-Based Learning_ könnte man von _Game-Development-Based Learning_ sprechen. 

> **Info:** Am Ende des Projekts, das je nach Komplexität der App 10 bis 20 Stunden beansprucht, steht die Veröffentlichung in die bekannten App Stores (Apple, Google). 
