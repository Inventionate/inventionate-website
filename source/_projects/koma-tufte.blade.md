---
extends: _layouts.project
section: content
title: KOMA-Tufte
subtitle: Die moderne Tufte-LaTeX-Klasse
start_date: 2018-01-01
description: Die Arbeiten von Eduward R. Tufte sind wegweisend für die Präsentation von Informationen. Leider ist die Implementierung seines markanten Ansatzes in LaTeX veraltet und wird nicht aktiv gepflegt. KOMA-Tufte implementiert eine scrtufte LaTeX Klasse und ermöglicht eine moderne Umsetzung des Tufte-Layouts. R Markdown wird ebenfalls unterstützt.
image: "https://images.unsplash.com/photo-1568047571827-8c46fe611345?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1950&q=80"
---

[KOMA-Script](https://komascript.de) ist eine bekannte Sammlung von Paketen und Klassen für LaTeX. Mit **scrlayer-notecolumn** beinhaltet es auch eine moderne Möglichkeit zur Umsetzung von Marginalspalten. Da [Tufte-LaTeX](https://tufte-latex.github.io/tufte-latex/) aktuell nicht gepflegt wird, und zum Beispiel inkompatibel mit BibLaTeX ist, habe ich eine KOMA-Script Klasse erstellt. KOMA-Tufte ermöglicht die Umsetzung des markanten [Tufte Designs](https://www.edwardtufte.com/tufte/) in einer modernen LaTeX Umgebung. Hier ein kleiner Ausschnitt, der spezifische Anpassungen am klassischen Tufte-Layout umsetzt. Beispielsweise wird die freie Schriftart [Vollkorn](http://vollkorn-typeface.com) verwendet.

<img alt="KOMA Tufte Beispiel" src="/assets/img/koma-tufte-example.png" class="shadow-md">

Neben dem Einsatz als LaTeX-Klasse wurden Erweiterungen für [R Markdown](https://rmarkdown.rstudio.com) entwickelt. Damit ist es möglich, dynamische Dokumente zu verfassen. Die KOMA-Script Basis löst zugleich viele Probleme, die mit der veralteten LaTeX-Basis von [Tufte R Markdown](https://rstudio.github.io/tufte/) einhergehen. Alle Features des Tufte R-Pakets wurden repliziert.

> **Verfügbarkeit:** Das Projekt ist fertig entwickelt, steht momentan aber (noch) nicht frei zur Verfügung. Sobald eine Veröffentlichung auf GitLab und CTAN erfolgt ist, wird hier informiert.
