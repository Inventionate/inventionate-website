<nav id="js-nav-menu" class="nav-menu hidden lg:hidden">
    <ul class="my-0 list-none">
        <li class="pl-4 ml-0">
            <a
                title="{{ $page->siteName }} Projekte"
                href="/projekte"
                class="nav-menu__item hover:text-blue-500 {{ $page->isActive('/projekte') ? 'active text-blue' : '' }}"
            >Projekte</a>
        </li>
        <li class="pl-4 ml-0">
            <a
                title="{{ $page->siteName }} Publikationen"
                href="/publikationen"
                class="nav-menu__item hover:text-blue-500 {{ $page->isActive('/publikationen') ? 'active text-blue' : '' }}"
            >Publikationen</a>
        </li>
        <li class="pl-4 ml-0">
            <a
                title="{{ $page->siteName }} Vorträge"
                href="/vortraege"
                class="nav-menu__item hover:text-blue-500 {{ $page->isActive('/vortraege') ? 'active text-blue' : '' }}"
            >Vorträge</a>
        </li>
        <li class="pl-4 ml-0">
            <a
                title="{{ $page->siteName }} Workshops"
                href="/workshops"
                class="nav-menu__item hover:text-blue-500 {{ $page->isActive('/workshops') ? 'active text-blue' : '' }}"
            >Workshops</a>
        </li>
    </ul>
</nav>
