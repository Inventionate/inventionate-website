<nav class="hidden lg:flex items-center justify-end text-lg">
    <a title="{{ $page->siteName }} Projekte" href="/projekte"
        class="ml-6 text-gray-700 hover:text-blue-600 {{ $page->isActive('/projekte') ? 'active text-blue-600' : '' }}">
        Projekte
    </a>

    <a title="{{ $page->siteName }} Publikationen" href="/publikationen"
        class="ml-6 text-gray-700 hover:text-blue-600 {{ $page->isActive('/publikationen') ? 'active text-blue-600' : '' }}">
        Publikationen
    </a>

    <a title="{{ $page->siteName }} Vorträge" href="/vortraege"
        class="ml-6 text-gray-700 hover:text-blue-600 {{ $page->isActive('/vortraege') ? 'active text-blue-600' : '' }}">
        Vorträge
    </a>

    <a title="{{ $page->siteName }} Workshops" href="/workshops"
        class="ml-6 text-gray-700 hover:text-blue-600 {{ $page->isActive('/workshops') ? 'active text-blue-600' : '' }}">
        Workshops
    </a>
</nav>
