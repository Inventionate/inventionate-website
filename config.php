<?php

use Illuminate\Support\Str;

return [
    'baseUrl' => 'https://inventionate-website.test',
    'production' => false,
    'siteName' => 'Fabian Mundt / Inventionate',
    'siteDescription' => 'Persönliche Website von Fabian Mundt.',
    'siteAuthor' => 'Fabian Mundt',

    // collections
    'collections' => [
        'projects' => [
            'sort' => '-start_date',
            'end_date' => "heute",
            'featured' => false,
            'path' => 'projekte/{filename}',
        ],
        'workshops' => [
            'author' => 'Fabian Mundt',
            'sort' => '-start_date',
            'path' => 'workshops/{filename}',
        ],
        'talks' => [
            'author' => 'Fabian Mundt',
            'sort' => '-date',
            'path' => 'vortraege/{filename}',
        ],
        'publications' => [
            'author' => 'Fabian Mundt',
            'sort' => '-date',
            'path' => 'publikationen/{filename}',
        ],
        'publication_categories' => [
            'path' => '/publikationen/kategorien/{filename}',
            'publications' => function ($page, $allPublications) {
                return $allPublications->filter(function ($publication) use ($page) {
                    return $publication->categories ? in_array($page->getFilename(), $publication->categories, true) : false;
                });
            },
        ],
    ],

    // helpers 
    'getDate' => function ($page) {
        setlocale(LC_ALL, 'de_DE.UTF-8');
        return strftime('%e. %B %Y', $page->date);
    },
    'getStartDate' => function ($page) {
        setlocale(LC_ALL, 'de_DE.UTF-8');
        return strftime('%e. %B %Y', $page->start_date);
    },
    'getPeriod' => function ($page, $start_date, $end_date) {
        setlocale(LC_ALL, 'de_DE.UTF-8');
        if ($start_date === $end_date) 
        {
            $period = strftime('%e. %B %Y', $end_date);
        }
        else
        {
            $period = date( 'j.', $start_date) . " bis " . strftime('%e. %B %Y', $end_date);
        }

        return $period;
    },
    'getProjectsCount' => function ($page, $collection) {
        return $collection
            ->whereNotNull('projects')
            ->filter(function ($item) use ($page) {
                return in_array($page->title, $item->projects );
            })
            ->count();
    },
    'getExcerpt' => function ($page, $length = 255) {
        if ($page->excerpt) {
            return $page->excerpt;
        }

        $content = preg_split('/<!-- more -->/m', $page->getContent(), 2);
        $cleaned = trim(
            strip_tags(
                preg_replace(['/<pre>[\w\W]*?<\/pre>/', '/<h\d>[\w\W]*?<\/h\d>/'], '', $content[0]),
                '<code>'
            )
        );

        if (count($content) > 1) {
            return $content[0];
        }

        $truncated = substr($cleaned, 0, $length);

        if (substr_count($truncated, '<code>') > substr_count($truncated, '</code>')) {
            $truncated .= '</code>';
        }

        return strlen($cleaned) > $length
            ? preg_replace('/\s+?(\S+)?$/', '', $truncated) . '...'
            : $cleaned;
    },
    'isActive' => function ($page, $path) {
        return Str::endsWith(trimPath($page->getPath()), trimPath($path));
    },
];
